
import { NativeModules } from 'react-native';

const { RNExtendedCloudinary } = NativeModules;

export default RNExtendedCloudinary;
