
#import "RNExtendedCloudinary.h"
#import "NSObject+ObjectMap.h"

#define ERROR_NOT_CONFIGURED_KEY @"ERROR_NOT_CONFIGURED"
#define ERROR_UPLOAD_FAILED_KEY @"ERROR_UPLOAD_FAILED"
#define ERROR_FILE_NOT_EXIST_KEY @"ERROR_FILE_NOT_EXIST"
#define ERROR_NOT_CONFIGURED_MSG @"Cloudinary service is not configured correctly!"
#define ERROR_UPLOAD_FAILED_MSG @"Failed to upload!"
#define ERROR_FILE_NOT_EXIST_MSG @"File doesn't exist!"

#define UPLOAD_WITH_URL YES

@implementation RNExtendedCloudinary

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

- (NSArray<NSString *> *)supportedEvents
{
    return @[@"ProgressCompleted"];
}

RCT_EXPORT_METHOD(config:(NSString *)cloudName apiKey:(NSString *)apiKey apiSecret:(NSString *)apiSecret presetName:(NSString*)presetName) {
    NSString *cloudinaryUrl = [NSString stringWithFormat:@"cloudinary://%@:%@@%@?secure=true", apiKey, apiSecret, cloudName];
    CLDConfiguration *config = [[CLDConfiguration alloc] initWithCloudinaryUrl:cloudinaryUrl];
    CLDCloudinary *cloudinary = [[CLDCloudinary alloc] initWithConfiguration:config networkAdapter:NULL sessionConfiguration:NULL];
    self.cloudinary = cloudinary;
    self.presetName = presetName;
}

RCT_EXPORT_METHOD(share:(BOOL)shareExtension) {
    self.shareExtension = shareExtension;
}

RCT_EXPORT_METHOD(upload:(NSString *)path
                  index:(NSInteger)index
                  singature:(NSString *)signature
                  timestamp:(NSInteger)timestamp
                  context:(NSString *)context
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    self.resolve = resolve;
    self.reject = reject;
    
    NSData *data;
    NSURL *url;
    
    if (self.shareExtension) {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        data = [fileManager contentsAtPath:path];
    } else if (UPLOAD_WITH_URL) {
        url = [[NSURL alloc] initFileURLWithPath:path];
    } else {
        data = [NSData dataWithContentsOfFile:path];
        
        if (data == nil) {
            if([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                data = [[NSFileManager defaultManager] contentsAtPath:path];
            } else {
                NSError *error = [NSError errorWithDomain:@"RNExtendedCloudinary" code:0 userInfo:nil];
                reject(ERROR_FILE_NOT_EXIST_KEY, ERROR_FILE_NOT_EXIST_MSG, error);
                return;
            }
        }
    }
    
    if (self.cloudinary) {
        CLDUploadRequestParams *params = [[CLDUploadRequestParams alloc] init];
        CLDSignature *cldSignature = [[CLDSignature alloc] initWithSignature:signature timestamp:@(timestamp)];
        [params setSignatureWithSignature:cldSignature];
        [params setContext:context];
        [params setResourceTypeFromUrlResourceType:CLDUrlResourceTypeAuto];
        
        __block CGFloat baseProgress = 0;
        if (self.uploadRequests == nil) self.uploadRequests = [[NSMutableDictionary alloc] initWithCapacity:10];
        CLDUploadRequest *request;
        if (self.presetName == nil) {
            if (UPLOAD_WITH_URL && !self.shareExtension) {
                request = [[self.cloudinary createUploader] signedUploadLargeWithUrl:url
                                                                              params:params
                                                                           chunkSize:[CLDUploader defaultChunkSize]
                                                                            progress:^(NSProgress * progress) {
                                                                                NSLog(@"Uploading Progress: %f", progress.fractionCompleted);
                                                                                CGFloat roundedProgress = floorf(progress.fractionCompleted * 100) / 100;
                                                                                if (roundedProgress > baseProgress) {
                                                                                    [self sendEventWithName:@"ProgressCompleted"
                                                                                                       body:@{@"value": [NSNumber numberWithDouble:progress.fractionCompleted],
                                                                                                              @"index": [NSNumber numberWithInteger:index]}];
                                                                                    baseProgress = roundedProgress;
                                                                                }
                                                                            } completionHandler:^(CLDUploadResult * result, NSError * error) {
                                                                                if (error != nil) {
                                                                                    reject(ERROR_UPLOAD_FAILED_KEY, ERROR_UPLOAD_FAILED_MSG, error);
                                                                                } else {
                                                                                    resolve([result objectDictionary]);
                                                                                }
                                                                            }];
            } else {
                request = [[self.cloudinary createUploader] signedUploadWithData:data
                                                                          params:params
                                                                        progress:^(NSProgress * progress) {
                                                                            NSLog(@"Uploading Progress: %f", progress.fractionCompleted);
                                                                            CGFloat roundedProgress = floorf(progress.fractionCompleted * 100) / 100;
                                                                            if (roundedProgress > baseProgress) {
                                                                                [self sendEventWithName:@"ProgressCompleted"
                                                                                                   body:@{@"value": [NSNumber numberWithDouble:progress.fractionCompleted],
                                                                                                          @"index": [NSNumber numberWithInteger:index]}];
                                                                                baseProgress = roundedProgress;
                                                                            }
                                                                        } completionHandler:^(CLDUploadResult * result, NSError * error) {
                                                                            if (error != nil) {
                                                                                reject(ERROR_UPLOAD_FAILED_KEY, ERROR_UPLOAD_FAILED_MSG, error);
                                                                            } else {
                                                                                resolve([result objectDictionary]);
                                                                            }
                                                                        }];
            }
        } else {
            if (UPLOAD_WITH_URL && !self.shareExtension) {
                request = [[self.cloudinary createUploader] uploadLargeWithUrl:url
                                                                  uploadPreset:self.presetName
                                                                        params:params
                                                                     chunkSize:[CLDUploader defaultChunkSize]
                                                                      progress:^(NSProgress * progress) {
                                                                          NSLog(@"Uploading Progress: %f", progress.fractionCompleted);
                                                                          CGFloat roundedProgress = floorf(progress.fractionCompleted * 100) / 100;
                                                                          if (roundedProgress > baseProgress) {
                                                                              [self sendEventWithName:@"ProgressCompleted"
                                                                                                 body:@{@"value": [NSNumber numberWithDouble:progress.fractionCompleted],
                                                                                                        @"index": [NSNumber numberWithInteger:index]}];
                                                                              baseProgress = roundedProgress;
                                                                          }
                                                                      } completionHandler:^(CLDUploadResult * result, NSError * error) {
                                                                          if (error != nil) {
                                                                              reject(ERROR_UPLOAD_FAILED_KEY, ERROR_UPLOAD_FAILED_MSG, error);
                                                                          } else {
                                                                              resolve([result objectDictionary]);
                                                                          }
                                                                      }];
            } else {
                request = [[self.cloudinary createUploader] uploadWithData:data
                                                              uploadPreset:self.presetName
                                                                    params:params
                                                                  progress:^(NSProgress * progress) {
                                                                      NSLog(@"Uploading Progress: %f", progress.fractionCompleted);
                                                                      CGFloat roundedProgress = floorf(progress.fractionCompleted * 100) / 100;
                                                                      if (roundedProgress > baseProgress) {
                                                                          [self sendEventWithName:@"ProgressCompleted"
                                                                                             body:@{@"value": [NSNumber numberWithDouble:progress.fractionCompleted],
                                                                                                    @"index": [NSNumber numberWithInteger:index]}];
                                                                          baseProgress = roundedProgress;
                                                                      }
                                                                  } completionHandler:^(CLDUploadResult * result, NSError * error) {
                                                                      if (error != nil) {
                                                                          reject(ERROR_UPLOAD_FAILED_KEY, ERROR_UPLOAD_FAILED_MSG, error);
                                                                      } else {
                                                                          resolve([result objectDictionary]);
                                                                      }
                                                                  }];
            }
        }
        [self.uploadRequests setObject:request forKey:@(index)];
    } else {
        NSError *error = [NSError errorWithDomain:@"RNExtendedCloudinary" code:0 userInfo:nil];
        reject(ERROR_NOT_CONFIGURED_KEY, ERROR_NOT_CONFIGURED_MSG, error);
    }
}

RCT_EXPORT_METHOD(cancel:(NSInteger *)index
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    CLDUploadRequest *request = [self.uploadRequests objectForKey:[NSNumber numberWithInteger:index]];
    if (request != nil) {
        [request cancel];
        [self.uploadRequests removeObjectForKey:[NSNumber numberWithInteger:index]];
        resolve(@"Upload cancelled.");
    }
}

RCT_EXPORT_METHOD(pause:(NSInteger *)index
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    CLDUploadRequest *request = [self.uploadRequests objectForKey:[NSNumber numberWithInteger:index]];
    if (request != nil) {
        [request suspend];
    }
}

RCT_EXPORT_METHOD(resume:(NSInteger *)index
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
    CLDUploadRequest *request = [self.uploadRequests objectForKey:[NSNumber numberWithInteger:index]];
    if (request != nil) {
        [request resume];
    }
}

@end



