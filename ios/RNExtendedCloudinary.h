
#if __has_include("RCTBridgeModule.h")
//#import "RCTBridgeModule.h"
#import "RCTEventEmitter.h"
#else
//#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#endif

#import <Cloudinary/Cloudinary-Swift.h>

@interface RNExtendedCloudinary : RCTEventEmitter <RCTBridgeModule>

@property (nonatomic, strong) RCTPromiseResolveBlock resolve;
@property (nonatomic, strong) RCTPromiseRejectBlock reject;

@property (nonatomic, readwrite) CLDCloudinary *cloudinary;
@property (nonatomic, readwrite) NSString *presetName;
@property (nonatomic, readwrite) BOOL shareExtension;
@property (nonatomic, readwrite) NSMutableDictionary *uploadRequests;

@end
