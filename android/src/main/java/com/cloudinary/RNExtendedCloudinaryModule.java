package com.cloudinary;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cloudinary.android.LogLevel;
import com.cloudinary.android.MediaManager;
import com.cloudinary.android.UploadRequest;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.signed.Signature;
import com.cloudinary.android.signed.SignatureProvider;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.modules.core.RCTNativeAppEventEmitter;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RNExtendedCloudinaryModule extends ReactContextBaseJavaModule {

    private static final String TAG = "RNExtendedCloudinary";

    private final ReactApplicationContext reactContext;

    private static final String ERROR_NOT_CONFIGURED_KEY = "ERROR_NOT_CONFIGURED";
    private static final String ERROR_UPLOAD_FAILED_KEY = "ERROR_UPLOAD_FAILED";
    private static final String ERROR_FILE_NOT_EXIST_KEY = "ERROR_FILE_NOT_EXIST";
    private static final String ERROR_NOT_CONFIGURED_MSG = "Cloudinary service is not configured correctly!";
    private static final String ERROR_UPLOAD_FAILED_MSG = "Failed to upload!";
    private static final String ERROR_FILE_NOT_EXIST_MSG = "File doesn't exist!";

    private boolean useNativeMethod = false;

    private String mCloudName, mApiKey, mSecretKey, mPresetName;
    private String mSignature;
    private int mTimestamp;
    private boolean isInitialized = false;

    private Cloudinary mCloudinary;

    private int timerInterval = 300, timerCounter = 1, currentTimerIndex = 0;
    private Map<Integer, String> requestIds = new HashMap();

    RNExtendedCloudinaryModule(ReactApplicationContext reactContext) {
        super(reactContext);

        this.reactContext = reactContext;

        MediaManager.setLogLevel(LogLevel.DEBUG);
    }

    @Override
    public String getName() {
        return "RNExtendedCloudinary";
    }

    private void sendEvent(String eventName, @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
//                .getJSModule(RCTNativeAppEventEmitter.class)
                .emit(eventName, params);
    }

    /**
     * signed upload configuration
     */
    @SuppressWarnings("unused")
    @ReactMethod
    public void config(String cloudName, String apiKey) {
        mCloudName = cloudName;
        mApiKey = apiKey;
    }

    /**
     * unsigned upload configuration
     */
    @SuppressWarnings("unused")
    @ReactMethod
    public void config(String cloudName, String apiKey, String secretKey, String presetName) {
        mCloudName = cloudName;
        mApiKey = apiKey;

        mSecretKey = secretKey;
        mPresetName = presetName;

        if (useNativeMethod) {
            configCloudinary();
        }
    }

    /**
     *  as android module doesn't support context in signed upload, use core module
     */
    private void configCloudinary() {
        final Map config = new HashMap();
        config.put("cloud_name", mCloudName);
        if (mSecretKey != null && !mSecretKey.isEmpty()) {
            config.put("api_secret", mSecretKey);
        }

        mCloudinary = new Cloudinary(config);
    }

    @SuppressWarnings("unused")
    @ReactMethod
    public void upload(final String path, int index, final String signature, final int timestamp, String context, final Promise promise) {
        mSignature = signature;
        mTimestamp = timestamp;

        if (useNativeMethod) {
            uploadNative(path, index, context, promise);
            return;
        }

        MediaManager mediaManager;
        try {
            mediaManager = MediaManager.get();
        } catch (IllegalStateException e) {
            final Map config = new HashMap();
            config.put("cloud_name", mCloudName);
            if (mSecretKey != null && !mSecretKey.isEmpty()) {
                config.put("api_secret", mSecretKey);
            }

            MediaManager.init(reactContext, new SignatureProvider() {
                @Override
                public Signature provideSignature(Map options) {
                    return new Signature(mSignature, mApiKey, mTimestamp);
                }

                @Override
                public String getName() {
                    return mCloudName;
                }
            }, config);

            mediaManager = MediaManager.get();
        }

        UploadRequest request = mediaManager.upload(path);
        if (mPresetName != null && !mPresetName.isEmpty()) {
            request = request.unsigned(mPresetName);
        }
        if (context != null && !context.isEmpty()) {
            request = request.option("context", context);
        }

        final String requestId = upload(request, promise, index);
        requestIds.put(index, requestId);
    }

    private String upload(UploadRequest request, final Promise promise, final int index) {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                timerCounter ++;
                handler.postDelayed(this, timerInterval);
            }
        };
        handler.postDelayed(runnable, timerInterval);

        return request.callback(new UploadCallback() {
            @Override
            public void onStart(String requestId) {
                Log.d(TAG, "requestId = " + requestId);
            }

            @Override
            public void onProgress(String requestId, long bytes, long totalBytes) {
                if (BuildConfig.DEBUG) Log.d(TAG, String.format("Uploading progress: %f", (float)bytes / totalBytes));

                WritableMap writableMap = Arguments.createMap();
                writableMap.putDouble("value", (float)bytes / totalBytes);
                writableMap.putInt("index", index);

                if (currentTimerIndex < timerCounter || bytes == totalBytes) {
                    sendEvent("ProgressCompleted", writableMap);
                    currentTimerIndex = timerCounter;
                }
            }

            @Override
            public void onSuccess(String requestId, Map resultData) {
                if (requestId.equals(requestIds.get(index))) requestIds.remove(index);
                promise.resolve(CloudinarySerialize.objectToWritable(resultData));
                handler.removeCallbacks(runnable);
            }

            @Override
            public void onError(String requestId, ErrorInfo error) {
                if (requestId.equals(requestIds.get(index))) requestIds.remove(index);
                promise.reject(String.valueOf(error.getCode()), error.getDescription());
                handler.removeCallbacks(runnable);
            }

            @Override
            public void onReschedule(String requestId, ErrorInfo error) {
                promise.reject(String.valueOf(error.getCode()), error.getDescription());
                handler.removeCallbacks(runnable);
            }
        }).dispatch();
    }

    private void uploadNative(final String path, final int index, String context, final Promise promise) {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                timerCounter ++;
                handler.postDelayed(this, timerInterval);
            }
        };
        handler.postDelayed(runnable, timerInterval);

        final Map options = new HashMap();
        options.put("timestamp", mTimestamp);
        options.put("signature", mSignature);
        options.put("api_key", mApiKey);
        options.put("context", context);
        options.put("resource_type", "auto");

        final Uploader uploader = mCloudinary.uploader();

        new AsyncTask<Void, String, Map>() {
            @Override
            protected Map doInBackground(Void... voids) {
                try {
                    final File file = new File(path);
                    final Map result = uploader.uploadLarge(file, options, new ProgressCallback() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            if (BuildConfig.DEBUG) Log.d(TAG, String.format("Uploading progress: %f", (float)bytesUploaded / totalBytes));

                            final WritableMap writableMap = Arguments.createMap();
                            writableMap.putDouble("value", (float)bytesUploaded / totalBytes);
                            writableMap.putInt("index", index);

                            if (currentTimerIndex < timerCounter || bytesUploaded == totalBytes) {
                                sendEvent("ProgressCompleted", writableMap);
                                currentTimerIndex = timerCounter;
                            }
                        }
                    });
                    promise.resolve(CloudinarySerialize.objectToWritable(result));
                } catch (IOException e) {
                    promise.reject(ERROR_FILE_NOT_EXIST_KEY, e);
                } catch (RuntimeException e) {
                    promise.reject(ERROR_UPLOAD_FAILED_KEY, e);
                }
                handler.removeCallbacks(runnable);
                return null;
            }
        }.execute();
    }

    @ReactMethod
    @SuppressWarnings("unused")
    public void cancel(int index) {
        final String requestId = requestIds.get(index);
        if (requestId != null && !requestId.isEmpty()) {
            MediaManager.get().cancelRequest(requestId);
        }
    }
}
