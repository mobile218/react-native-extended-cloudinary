
# react-native-extended-cloudinary

## Getting started

`$ npm install react-native-extended-cloudinary --save`

### Mostly automatic installation

`$ react-native link react-native-extended-cloudinary`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-extended-cloudinary` and add `RNExtendedCloudinary.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNExtendedCloudinary.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNExtendedCloudinaryPackage;` to the imports at the top of the file
  - Add `new RNExtendedCloudinaryPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-extended-cloudinary'
  	project(':react-native-extended-cloudinary').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-extended-cloudinary/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-extended-cloudinary')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNExtendedCloudinary.sln` in `node_modules/react-native-extended-cloudinary/windows/RNExtendedCloudinary.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Extended.Cloudinary.RNExtendedCloudinary;` to the usings at the top of the file
  - Add `new RNExtendedCloudinaryPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNExtendedCloudinary from 'react-native-extended-cloudinary';

// TODO: What to do with the module?
RNExtendedCloudinary;
```
  